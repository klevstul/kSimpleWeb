#!C:/prgFiles/perl/bin/Perl.exe
#!/usr/bin/perl

# ---------------------------------------------
# filename:	createPage.cgi
# author:	Frode Klevstul (frode@klevstul.com)
# started:	29.10.2004
# version:	v01_20050529
# ---------------------------------------------


# ----------------------------------
# use
# ----------------------------------
use strict;
use kSimpleWeb;

# ------------------
# declarations
# ------------------
my $file		= $ARGV[0];
my $dir_import	= "import";

# ------------------
# main
# ------------------
if ($file ne ""){
	&kSimpleWeb_printPage("top",$file);
	&index_printBody;
	&kSimpleWeb_printPage("bottom",$file);
}


# ------------------
# sub
# ------------------

sub index_printBody{
	my $line;
    open(FILE, "<$dir_import/$file") || die("failed to open '$dir_import/$file'");
	while ($line = <FILE>){
		print $line;
	}
	close (FILE);
}
